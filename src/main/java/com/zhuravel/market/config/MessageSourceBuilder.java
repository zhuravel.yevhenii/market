package com.zhuravel.market.config;

import org.springframework.context.MessageSource;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

/**
 * @author Evgenii Zhuravel created on 03.06.2022
 * @version 1.0
 */
public class MessageSourceBuilder {

    public static MessageSource build() {
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();

        messageSource.setBasename("classpath:messages");
        messageSource.setDefaultEncoding("utf-8");
        return messageSource;
    }
}
