package com.zhuravel.market.config;

import org.springframework.boot.autoconfigure.security.servlet.PathRequest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.rememberme.InMemoryTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import static com.zhuravel.market.config.Constants.Mapping.MAPPING_CATALOG;
import static com.zhuravel.market.config.Constants.Mapping.MAPPING_LOGIN;
import static com.zhuravel.market.config.Constants.Mapping.MAPPING_PRODUCT_PAGE;
import static com.zhuravel.market.config.Constants.Mapping.MAPPING_SHOPPING_CART;
import static com.zhuravel.market.config.Constants.Mapping.MAPPING_SUCCESS;

/**
 * @author Evgenii Zhuravel created on 23.05.2022
 * @version 1.0
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfig {

    @Bean
    protected SecurityFilterChain configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .authorizeRequests()
                .requestMatchers(PathRequest.toStaticResources().atCommonLocations(), new AntPathRequestMatcher("/fonts/**")).permitAll()
                .requestMatchers(PathRequest.toStaticResources().atCommonLocations(), new AntPathRequestMatcher("/img/**")).permitAll()
                .antMatchers("/", "/registration**", MAPPING_CATALOG + "/**", MAPPING_PRODUCT_PAGE + "/**").permitAll()
                .antMatchers(HttpMethod.GET, "/categories/**").permitAll()
                // For ADMIN only.
                .antMatchers("/admin", "/users").hasRole("ADMIN")
                // For ROLE_USER or ROLE_ADMIN.
                .antMatchers(MAPPING_SHOPPING_CART).hasAnyRole("USER", "ADMIN")
                .anyRequest().authenticated()
                // Config for Login Form
                .and().formLogin().loginPage(MAPPING_LOGIN).permitAll()
                .defaultSuccessUrl(MAPPING_LOGIN + MAPPING_SUCCESS)
                .usernameParameter("login")
                .passwordParameter("password")
                // Config for Logout Page
                .and().logout()
                .logoutUrl("/logout").logoutSuccessUrl(MAPPING_CATALOG);

        // Config Remember Me.
        http.authorizeRequests().and() //
                .rememberMe().tokenRepository(this.persistentTokenRepository()) //
                .tokenValiditySeconds(1 * 24 * 60 * 60); // 24h*/

        return http.build();
    }

    // Token stored in Memory (Of Web Server).
    @Bean
    public PersistentTokenRepository persistentTokenRepository() {
        return new InMemoryTokenRepositoryImpl();
    }

    @Bean
    public AuthenticationManager authenticationManager(AuthenticationConfiguration authenticationConfiguration) throws Exception {
        return authenticationConfiguration.getAuthenticationManager();
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
