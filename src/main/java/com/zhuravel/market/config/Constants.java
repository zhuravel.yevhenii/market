package com.zhuravel.market.config;

/**
 * @author Evgenii Zhuravel created on 31.05.2022
 * @version 1.0
 */
public class Constants {

    public static final class Mapping {

        public static final String MAPPING_LOGIN = "/login";
        public static final String MAPPING_SUCCESS = "/success";
        public static final String MAPPING_SHOPPING_CART = "/shopping-cart";
        public static final String MAPPING_CATALOG = "/catalog";
        public static final String MAPPING_PRODUCT_PAGE = "/product-page";
        public static final String MAPPING_CATEGORIES = "/admin/categories";

        public static final String MAPPING_CATEGORIES_REDIRECT = "redirect:" + MAPPING_CATEGORIES  + "/";
        public static final String REDIRECT_CATALOG = "redirect:/catalog";
    }

    public static final class Attributes {

        public static final String ATTRIBUTE_PRODUCT_LIST = "productList";
        public static final String FILTER_CRITERIA = "filterCriteria";
        public static final String CURRENT_USER = "currentUser";
        public static final String ATTRIBUTE_SELECTED_PRODUCTS = "selectedProducts";
        public static final String ATTRIBUTE_CATEGORIES = "categories";
        public static final String ATTRIBUTE_PARENT_CATEGORY = "parentCategory";
        public static final String ATTRIBUTE_PARENT_CATEGORIES = "upperCategories";
    }
}
