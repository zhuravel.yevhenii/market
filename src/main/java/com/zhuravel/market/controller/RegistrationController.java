package com.zhuravel.market.controller;

import com.zhuravel.market.model.LocaleConvertor;
import com.zhuravel.market.model.RoleType;
import com.zhuravel.market.model.UserStatusType;
import com.zhuravel.market.model.entity.Role;
import com.zhuravel.market.model.entity.User;
import com.zhuravel.market.model.entity.UserStatus;
import com.zhuravel.market.service.SecurityService;
import com.zhuravel.market.service.UserService;
import com.zhuravel.market.validator.UserValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

/**
 * @author Evgenii Zhuravel created on 22.05.2022
 * @version 1.0
 */
@Controller
public class RegistrationController {

    private static final Logger logger = LoggerFactory.getLogger(RegistrationController.class);

    private static final String PAGE_REGISTRATION = "registration";
    private static final String PAGE_ADMIN = "pages/admin";

    private static final String MAPPING_REGISTRATION = "/registration";
    private static final String MAPPING_INDEX = "/";
    private static final String MAPPING_ADMIN = "/admin";

    private static final String ATTRIBUTE_USER_FORM = "userForm";

    private final UserService userService;

    private final SecurityService securityService;

    private final UserValidator userValidator;

    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public RegistrationController(UserService userService, SecurityService securityService, UserValidator userValidator, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userService = userService;
        this.securityService = securityService;
        this.userValidator = userValidator;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @GetMapping(MAPPING_REGISTRATION)
    public String registration(Model model) {
        logger.info("RegistrationController.registration(model) called");
        model.addAttribute(ATTRIBUTE_USER_FORM, new User());

        return PAGE_REGISTRATION;
    }

    @PostMapping(MAPPING_REGISTRATION)
    public String registration(@Valid @ModelAttribute(ATTRIBUTE_USER_FORM) User userForm, BindingResult bindingResult) {
        logger.info("RegistrationController.registration(userForm, bindingResult) called");
        userForm.setPassword(bCryptPasswordEncoder.encode(userForm.getPassword()));
        userForm.setRole(new Role(RoleType.USER));
        userForm.setStatus(new UserStatus(UserStatusType.ACTIVE));
        userForm.setLocale(LocaleConvertor.getLocaleType(LocaleContextHolder.getLocale()));

        userValidator.validate(userForm, bindingResult);

        if (bindingResult.hasErrors()) {
            return PAGE_REGISTRATION;
        }

        userService.save(userForm);

        securityService.autoLogin(userForm.getPassword(), userForm.getConfirmPassword());

        return "redirect:" + MAPPING_INDEX;
    }

    @GetMapping(MAPPING_ADMIN)
    public String admin() {
        logger.info("RegistrationController.admin() called");
        return PAGE_ADMIN;
    }
}
