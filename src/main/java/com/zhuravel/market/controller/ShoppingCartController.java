package com.zhuravel.market.controller;

import com.zhuravel.market.service.UserService;
import com.zhuravel.market.service.dto.SelectedProductsDto;
import com.zhuravel.market.service.dto.ShoppingCartDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;

import static com.zhuravel.market.config.Constants.Attributes.ATTRIBUTE_SELECTED_PRODUCTS;
import static com.zhuravel.market.config.Constants.Mapping.MAPPING_SHOPPING_CART;
import static com.zhuravel.market.config.Constants.Mapping.REDIRECT_CATALOG;

/**
 * @author Evgenii Zhuravel created on 03.06.2022
 * @version 1.0
 */
@Controller
@RequestMapping(MAPPING_SHOPPING_CART)
public class ShoppingCartController {

    private static final Logger logger = LoggerFactory.getLogger(ShoppingCartController.class);

    private static final String PAGE_SHOPPING_CART = "shopping_cart";

    public static final String ATTRIBUTE_PRODUCT_CART = "productCart";
    public static final String ATTRIBUTE_CHECKOUTS = "checkouts";

    private final UserService userService;

    @Autowired
    public ShoppingCartController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/add/{productId}")
    public String addProductToShoppingCart(Authentication authentication,
                                           @PathVariable("productId") Long productId,
                                           @RequestParam("url") String url,
                                           HttpSession session) {
        logger.info("ShoppingCartController.addProductToShoppingCart(authentication, productId, url, session)");
        logger.info("url " + url);

        String currentUserLogin = authentication.getName();
        userService.add(currentUserLogin, productId);

        SelectedProductsDto selectedProducts = (SelectedProductsDto) session.getAttribute(ATTRIBUTE_SELECTED_PRODUCTS);
        selectedProducts.getQuantities().put(productId, 1);

        return "redirect:" + url;
    }

    @GetMapping
    public String getShoppingCart(Model model, HttpSession session, Authentication authentication) {
        logger.info("ShoppingCartController.getShoppingCart(model, session, authentication)");

        String currentUserName = authentication.getName();
        ShoppingCartDto products = userService.getShoppingCart(currentUserName);

        session.setAttribute(ATTRIBUTE_PRODUCT_CART, products);
        session.setAttribute(ATTRIBUTE_SELECTED_PRODUCTS, products.getSelectedProducts());

        model.addAttribute(ATTRIBUTE_PRODUCT_CART, products);
        model.addAttribute(ATTRIBUTE_SELECTED_PRODUCTS, products.getSelectedProducts());
        model.addAttribute(ATTRIBUTE_CHECKOUTS, products.getCheckouts());

        return PAGE_SHOPPING_CART;
    }

    @PostMapping(params = "save")
    public String saveShoppingCart(HttpSession session,
                                   @ModelAttribute(ATTRIBUTE_SELECTED_PRODUCTS) SelectedProductsDto form) {
        logger.info("ShoppingCartController.saveShoppingCart(session, form)");

        ShoppingCartDto cartDto = (ShoppingCartDto) session.getAttribute(ATTRIBUTE_PRODUCT_CART);
        cartDto.setSelectedProducts(form);

        userService.updateShoppingCart(cartDto);

        return REDIRECT_CATALOG;
    }

    @PostMapping(params = "submit")
    public String checkout(HttpSession session,
                                   @ModelAttribute(ATTRIBUTE_SELECTED_PRODUCTS) SelectedProductsDto form) {
        logger.info("ShoppingCartController.checkout(session, form)");

        ShoppingCartDto cartDto = (ShoppingCartDto) session.getAttribute(ATTRIBUTE_PRODUCT_CART);
        cartDto.setSelectedProducts(form);

        userService.checkout(cartDto);

        return REDIRECT_CATALOG;
    }

    @GetMapping("/checkout/{date}")
    public String getCheckout(Model model,
                              @PathVariable("date") String date) {
        logger.info("ShoppingCartController.getCheckout(model, date)");

        return PAGE_SHOPPING_CART;
    }
}
