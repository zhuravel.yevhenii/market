package com.zhuravel.market.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author Evgenii Zhuravel created on 27.05.2022
 * @version 1.0
 */
@Controller
public class InternationalController {

    @GetMapping("/international")
    public String getInternationalPage() {
        return "international";
    }
}
