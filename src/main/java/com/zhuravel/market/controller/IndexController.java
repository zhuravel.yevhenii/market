package com.zhuravel.market.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import static com.zhuravel.market.config.Constants.Mapping.REDIRECT_CATALOG;

/**
 * @author Evgenii Zhuravel created on 04.06.2022
 * @version 1.0
 */
@Controller
public class IndexController {

    private static final Logger logger = LoggerFactory.getLogger(IndexController.class);

    @RequestMapping({"/", ""})
    public String rootCategory() {
        logger.info("Index path");
        return REDIRECT_CATALOG;
    }
}
