package com.zhuravel.market.controller;

import com.zhuravel.market.service.UserService;
import com.zhuravel.market.service.dto.ShoppingCartDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

import static com.zhuravel.market.config.Constants.Attributes.ATTRIBUTE_SELECTED_PRODUCTS;
import static com.zhuravel.market.config.Constants.Mapping.MAPPING_LOGIN;
import static com.zhuravel.market.config.Constants.Mapping.MAPPING_SUCCESS;
import static com.zhuravel.market.config.Constants.Mapping.REDIRECT_CATALOG;

/**
 * @author Evgenii Zhuravel created on 22.05.2022
 * @version 1.0
 */
@Controller
@RequestMapping(MAPPING_LOGIN)
public class LoginController {

    private static final Logger logger = LoggerFactory.getLogger(LoginController.class);

    private static final String PAGE_LOGIN = "login";
    private static final String PAGE_INDEX = "pages/index";

    private static final String ATTRIBUTE_ERROR = "error";
    private static final String ATTRIBUTE_MESSAGE = "message";

    private final MessageSource messageSource;

    private final UserService userService;

    @Autowired
    public LoginController(MessageSource messageSource, UserService userService) {
        this.messageSource = messageSource;
        this.userService = userService;
    }

    @GetMapping
    public String loginGet(Model model, String error, String logout) {
        logger.debug("Received GET request to login form");

        if (error != null) {
            model.addAttribute(ATTRIBUTE_ERROR,
                    messageSource.getMessage("userForm.loginOrPass.incorrect",
                            null, LocaleContextHolder.getLocale()));
        }

        if (logout != null) {
            model.addAttribute(ATTRIBUTE_MESSAGE,
                    messageSource.getMessage("userForm.logout.successful",
                            null, LocaleContextHolder.getLocale()));
        }

        return PAGE_LOGIN;
    }

    @GetMapping(MAPPING_SUCCESS)
    public String loginSuccess(HttpSession session,
                               Authentication authentication) {
        String currentUserName = authentication.getName();

        ShoppingCartDto products = userService.getShoppingCart(currentUserName);

        session.setAttribute(ATTRIBUTE_SELECTED_PRODUCTS, products.getSelectedProducts());

        return REDIRECT_CATALOG;
    }

    @PostMapping({MAPPING_LOGIN})
    public String loginPost() {
        logger.debug("Received POST request to login form");
        return PAGE_INDEX;
    }
}
