package com.zhuravel.market.controller;

import com.zhuravel.market.model.entity.LocalizedCategory;
import com.zhuravel.market.model.entity.LocalizedParameterType;
import com.zhuravel.market.model.entity.Product;
import com.zhuravel.market.service.LocalizedCategoryService;
import com.zhuravel.market.service.ParameterTypeService;
import com.zhuravel.market.service.ProductService;
import com.zhuravel.market.service.dto.ProductsCatalogDto;
import com.zhuravel.market.service.dto.SelectedParametersDto;
import com.zhuravel.market.service.dto.SelectedParametersDtoBuilder;
import com.zhuravel.market.service.filter_criteria.ParameterSearchCriteria;
import com.zhuravel.market.service.filter_criteria.ParameterSearchCriteriaBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.zhuravel.market.config.Constants.Attributes.ATTRIBUTE_PARENT_CATEGORIES;
import static com.zhuravel.market.config.Constants.Attributes.ATTRIBUTE_PRODUCT_LIST;
import static com.zhuravel.market.config.Constants.Attributes.FILTER_CRITERIA;
import static com.zhuravel.market.config.Constants.Mapping.MAPPING_CATALOG;
import static com.zhuravel.market.config.Constants.Mapping.REDIRECT_CATALOG;

/**
 * @author Evgenii Zhuravel created on 19.05.2022
 * @version 1.0
 */
@Controller
@RequestMapping(MAPPING_CATALOG)
public class CatalogController {

    private static final Logger logger = LoggerFactory.getLogger(CatalogController.class);

    private static final String PAGE_CATEGORIES = "/categories";
    private static final String PAGE_CATALOG = "/catalog";

    private static final String ATTRIBUTE_CATEGORY = "category";
    private static final String ATTRIBUTE_CATEGORY_LIST = "categoryList";
    private static final String ATTRIBUTE_PARENT_CATEGORY = "parentCategory";

    private static final String DEFAULT_PAGE = "1";
    private static final String DEFAULT_SIZE = "10";

    private final LocalizedCategoryService localizedCategoryService;
    private final ProductService productService;
    private final ParameterTypeService parameterTypeService;

    @Autowired
    public CatalogController(LocalizedCategoryService localizedCategoryService,
                             ProductService productService,
                             ParameterTypeService parameterTypeService) {
        this.localizedCategoryService = localizedCategoryService;
        this.productService = productService;
        this.parameterTypeService = parameterTypeService;
    }

    @GetMapping({"/"})
    public String index() {
        logger.info("Get index Categories");
        return REDIRECT_CATALOG;
    }

    @GetMapping
    public String rootCategory(Model model) {
        logger.info("Get root Categories");

        List<LocalizedCategory> categories = localizedCategoryService.getSubCategories(0L);
        LocalizedCategory category = localizedCategoryService.getById(0L);

        model.addAttribute(ATTRIBUTE_CATEGORY, category);
        model.addAttribute(ATTRIBUTE_PARENT_CATEGORIES, categories);
        model.addAttribute(ATTRIBUTE_CATEGORY_LIST, categories);

        return PAGE_CATEGORIES;
    }

    @GetMapping("/{id}")
    public String getCatalog(Model model,
                             @PathVariable("id") Long categoryId,
                             @ModelAttribute(FILTER_CRITERIA) SelectedParametersDto selectedParameters,
                             @RequestParam(value = "page", defaultValue = DEFAULT_PAGE) int page,
                             @RequestParam(value = "size", defaultValue = DEFAULT_SIZE) int size) {
        logger.info("Get Category with id: " + categoryId);

        if (categoryId == 0) {
            return REDIRECT_CATALOG;
        }

        return prepareCatalogPage(model, categoryId, PageRequest.of(page-1, size), new SelectedParametersDto());
    }

    @PostMapping("/{id}")
    public String getFilteredCatalog(Model model,
                                     @PathVariable("id") long categoryId,
                                     @ModelAttribute(FILTER_CRITERIA) SelectedParametersDto selectedParameters,
                                     @RequestParam(value = "page", defaultValue = DEFAULT_PAGE) int page,
                                     @RequestParam(value = "size", defaultValue = DEFAULT_SIZE) int size) {
        logger.info("Get Filtered Category with id: " + categoryId);

        return prepareCatalogPage(model, categoryId, PageRequest.of(page-1, size), selectedParameters);
    }

    private String prepareCatalogPage(Model model, long categoryId, Pageable pageable, SelectedParametersDto selectedParameters) {
        LocalizedCategory localizedCategory = localizedCategoryService.getById(categoryId);
        List<LocalizedCategory> categories = localizedCategoryService.getSubCategories(categoryId);

        Long parentCategoryId = localizedCategory.getCategory().getParentCategoryId();

        model.addAttribute(ATTRIBUTE_CATEGORY, localizedCategory);
        model.addAttribute(ATTRIBUTE_PARENT_CATEGORIES, localizedCategoryService.getSubCategories(parentCategoryId));

        if (parentCategoryId != 0) {
            model.addAttribute(ATTRIBUTE_PARENT_CATEGORY, localizedCategoryService.getById(parentCategoryId));
        }

        if (categories.size() > 0) {
            model.addAttribute(ATTRIBUTE_CATEGORY_LIST, categories);

            return PAGE_CATEGORIES;
        } else {
            return getCatalogPage(model, categoryId, pageable, selectedParameters);
        }
    }

    private String getCatalogPage(Model model, long categoryId, Pageable pageable, SelectedParametersDto selectedParameters) {
        ProductsCatalogDto productsCatalogDTO;

        if (selectedParameters.getParameterTypes().size() == 0) {
            List<LocalizedParameterType> parameterTypes = parameterTypeService.getAllByCategoryId(categoryId);

            selectedParameters = SelectedParametersDtoBuilder.build(parameterTypes);
        }

        ParameterSearchCriteria criteria = ParameterSearchCriteriaBuilder.build(categoryId, selectedParameters);
        productsCatalogDTO = productService.getAll(criteria, pageable);

        if (selectedParameters.getProducers() == null) {
            SelectedParametersDtoBuilder.setProducers(selectedParameters, productsCatalogDTO.getProducers());
        }

        Page<Product> products = productsCatalogDTO.getProducts();
        int totalPages = products.getTotalPages();

        List<Integer> pageNumbers = new ArrayList<>();

        if (totalPages > 0) {
            pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
        }

        model.addAttribute("pageNumbers", pageNumbers);
        model.addAttribute(ATTRIBUTE_PRODUCT_LIST, products);
        model.addAttribute(FILTER_CRITERIA, selectedParameters);

        return PAGE_CATALOG;
    }
}
