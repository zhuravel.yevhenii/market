package com.zhuravel.market.controller.admin;

import com.zhuravel.market.model.entity.Category;
import com.zhuravel.market.model.entity.LocalizedCategory;
import com.zhuravel.market.service.CategoryService;
import com.zhuravel.market.service.LocalizedCategoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

import static com.zhuravel.market.config.Constants.Attributes.ATTRIBUTE_CATEGORIES;
import static com.zhuravel.market.config.Constants.Attributes.ATTRIBUTE_PARENT_CATEGORIES;
import static com.zhuravel.market.config.Constants.Attributes.ATTRIBUTE_PARENT_CATEGORY;
import static com.zhuravel.market.config.Constants.Mapping.MAPPING_CATEGORIES;
import static com.zhuravel.market.config.Constants.Mapping.MAPPING_CATEGORIES_REDIRECT;

/**
 * @author Evgenii Zhuravel created on 12.07.2022
 * @version 1.0
 */
@Controller
@RequestMapping(MAPPING_CATEGORIES)
public class CategoriesController {

    private static final Logger logger = LoggerFactory.getLogger(CategoriesController.class);

    private static final String PAGE_CATEGORIES = "pages/admin/admin-categories";

    private static final String ATTRIBUTE_CATEGORY = "category";
    private static final String ATTRIBUTE_CATEGORY_FORM = "categoryForm";
    private static final String ATTRIBUTE_CATEGORY_LIST = "listCategories";

    private final CategoryService categoryService;
    private final LocalizedCategoryService localizedCategoryService;

    @Autowired
    public CategoriesController(CategoryService categoryService, LocalizedCategoryService localizedCategoryService) {
        this.categoryService = categoryService;
        this.localizedCategoryService = localizedCategoryService;
    }

    @GetMapping({"/", ""})
    public String getAllCategories(Model model) {
        logger.info("CategoriesController.getAllCategories(model) called");

        return getCategory(0L, model);
    }

    @GetMapping("/{id}")
    public String getCategory(@PathVariable("id") long id, Model model) {
        logger.info("CategoriesController.getCategory(id, model) called with id: " + id);

        LocalizedCategory category = localizedCategoryService.getById(id);
        Long parentCategoryId = category.getCategory().getParentCategoryId();
        List<Category> categories = categoryService.getSubCategories(id);
        List<LocalizedCategory> subCategories;

        if (parentCategoryId != null && parentCategoryId != 0) {
            subCategories = localizedCategoryService.getSubCategories(parentCategoryId);
            model.addAttribute(ATTRIBUTE_PARENT_CATEGORY, localizedCategoryService.getById(parentCategoryId));
        } else {
            subCategories = localizedCategoryService.getSubCategories(id);
        }

        Category form = new Category();
        form.setTranslations(List.of(new LocalizedCategory(), new LocalizedCategory()));

        model.addAttribute(ATTRIBUTE_CATEGORY, category);
        model.addAttribute(ATTRIBUTE_CATEGORIES, categories);
        model.addAttribute(ATTRIBUTE_CATEGORY_FORM, form);
        model.addAttribute(ATTRIBUTE_PARENT_CATEGORIES, subCategories);

        return PAGE_CATEGORIES;
    }

    @PostMapping("/add")
    public String addCategory(@ModelAttribute(ATTRIBUTE_CATEGORY_FORM) Category category) {
        logger.info("CategoriesController.addCategory(category) called with category: " + category);

        if (category.getId() == null) {
            categoryService.save(category);
        } else {
            categoryService.update(category);
        }

        return MAPPING_CATEGORIES_REDIRECT + category.getParentCategoryId();
    }

    @GetMapping("/{parentId}/remove/{id}")
    public String removeCategory(@PathVariable("id") long id, @PathVariable("parentId") long parentId) {
        logger.info("CategoriesController.removeCategory(id, parentId) called with id: " + id);

        categoryService.delete(id);

        return MAPPING_CATEGORIES_REDIRECT + parentId;
    }

    @GetMapping("/{parentId}/edit/{id}")
    public String editCategory(@PathVariable("id") long id, @PathVariable("parentId") long parentId, Model model) {
        logger.info("CategoriesController.editCategory(id, parentId, model) called with id: " + id);

        LocalizedCategory parentCategory = localizedCategoryService.getById(parentId);
        Category category = categoryService.getById(id);
        model.addAttribute(ATTRIBUTE_CATEGORY, parentCategory);
        model.addAttribute(ATTRIBUTE_CATEGORY_FORM, category);
        model.addAttribute(ATTRIBUTE_CATEGORY_LIST, categoryService.getSubCategories(parentId));

        return PAGE_CATEGORIES;
    }
}
