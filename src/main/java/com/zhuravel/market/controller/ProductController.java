package com.zhuravel.market.controller;

import com.zhuravel.market.model.entity.Product;
import com.zhuravel.market.service.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import static com.zhuravel.market.config.Constants.Mapping.MAPPING_PRODUCT_PAGE;

/**
 * @author Evgenii Zhuravel created on 07.06.2022
 * @version 1.0
 */
@Controller
@RequestMapping(MAPPING_PRODUCT_PAGE)
public class ProductController {
    private static final Logger logger = LoggerFactory.getLogger(ProductController.class);

    private static final String PAGE_PRODUCT = "product-page";

    public static final String ATTRIBUTE_PRODUCT = "product";

    private final ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/{productId}")
    public String getProductCart(Model model,
                                 @PathVariable("productId") Long productId) {
        logger.debug("Get product cart");

        Product product = productService.getById(productId);

        model.addAttribute(ATTRIBUTE_PRODUCT, product);

        return PAGE_PRODUCT;
    }
}
