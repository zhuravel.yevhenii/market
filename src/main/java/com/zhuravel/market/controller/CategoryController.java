package com.zhuravel.market.controller;

import com.zhuravel.market.model.entity.Category;
import com.zhuravel.market.model.entity.LocalizedCategory;
import com.zhuravel.market.service.LocalizedCategoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * @author Evgenii Zhuravel created on 19.05.2022
 * @version 1.0
 */
@Controller
@RequestMapping("/categories")
public class CategoryController {

    private static final Logger logger = LoggerFactory.getLogger(CategoryController.class);

    private static final String PAGE_CATEGORIES = "pages/categories";

    private static final String ATTRIBUTE_CATEGORY = "category";
    private static final String ATTRIBUTE_CATEGORY_FORM = "categoryForm";
    private static final String ATTRIBUTE_CATEGORY_LIST = "listCategories";

    private static final String REDIRECT_CATEGORIES = "redirect:/categories/";

    private LocalizedCategoryService localizedCategoryService;

    @Autowired
    public void setCategoryService(LocalizedCategoryService localizedCategoryService) {
        this.localizedCategoryService = localizedCategoryService;
    }

    @GetMapping("/0")
    public String rootCategory() {
        logger.info("Root Category");

        return REDIRECT_CATEGORIES;
    }

    @GetMapping({"/", ""})
    public String getAllCategories(Model model) {
        logger.info("Get all Categories");

        LocalizedCategory category = localizedCategoryService.getById(0L);
        List<LocalizedCategory> categories = localizedCategoryService.getSubCategories(0L);
        model.addAttribute(ATTRIBUTE_CATEGORY, category);
        model.addAttribute(ATTRIBUTE_CATEGORY_LIST, categories);
        model.addAttribute(ATTRIBUTE_CATEGORY_FORM, new Category());

        return PAGE_CATEGORIES;
    }

    @GetMapping("/{id}")
    public String getCategory(@PathVariable("id") long id, Model model) {
        logger.info("Get Category with id: " + id);

        model.addAttribute(ATTRIBUTE_CATEGORY, localizedCategoryService.getById(id));
        model.addAttribute(ATTRIBUTE_CATEGORY_LIST, localizedCategoryService.getSubCategories(id));
        model.addAttribute(ATTRIBUTE_CATEGORY_FORM, new LocalizedCategory());

        return PAGE_CATEGORIES;
    }

    @PostMapping("/add")
    public String addCategory(@ModelAttribute(ATTRIBUTE_CATEGORY_FORM) LocalizedCategory category) {
        logger.info("Add new Category: " + category);

        if (category.getLocalizedId().getId() == null) {
            localizedCategoryService.save(category);
        } else {
            localizedCategoryService.update(category);
        }

        return REDIRECT_CATEGORIES + category.getCategory().getParentCategoryId();
    }

    @GetMapping("/{parentId}/remove/{id}")
    public String removeCategory(@PathVariable("id") long id, @PathVariable("parentId") long parentId) {
        logger.info("Remove Category with id: " + id);

        localizedCategoryService.delete(id);

        return REDIRECT_CATEGORIES + parentId;
    }

    @GetMapping("/{parentId}/edit/{id}")
    public String editCategory(@PathVariable("id") long id, @PathVariable("parentId") long parentId, Model model) {
        logger.info("Edit Category with id: " + id);

        LocalizedCategory parentCategory = localizedCategoryService.getById(parentId);
        LocalizedCategory category = localizedCategoryService.getById(id);
        model.addAttribute(ATTRIBUTE_CATEGORY, parentCategory);
        model.addAttribute(ATTRIBUTE_CATEGORY_FORM, category);
        model.addAttribute(ATTRIBUTE_CATEGORY_LIST, localizedCategoryService.getSubCategories(parentId));

        return PAGE_CATEGORIES;
    }
}
