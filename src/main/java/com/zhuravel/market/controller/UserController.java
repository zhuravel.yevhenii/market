package com.zhuravel.market.controller;

import com.zhuravel.market.model.RoleType;
import com.zhuravel.market.model.UserStatusType;
import com.zhuravel.market.model.entity.Role;
import com.zhuravel.market.model.entity.User;
import com.zhuravel.market.model.entity.UserStatus;
import com.zhuravel.market.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

import static com.zhuravel.market.controller.UserController.MAPPING_USERS;

/**
 * @author Evgenii Zhuravel created on 22.05.2022
 * @version 1.0
 */
@Controller
@RequestMapping(MAPPING_USERS)
public class UserController {

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    protected static final String MAPPING_USERS = "/users";

    private static final String PAGE_USERS = "pages/users";

    private static final String ATTRIBUTE_USER_FORM = "userForm";
    private static final String ATTRIBUTE_USER_LIST = "listUsers";
    private static final String ATTRIBUTE_USER = "user";

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public String getAllUsers(Model model) {
        logger.info("Get all Users");

        List<User> users = userService.getAll();
        model.addAttribute(ATTRIBUTE_USER_LIST, users);
        model.addAttribute(ATTRIBUTE_USER_FORM, new User());

        return PAGE_USERS;
    }

    @GetMapping("/{id}")
    public String getUser(@PathVariable("id") long id, Model model) {
        logger.info("Get User with id: " + id);

        model.addAttribute(ATTRIBUTE_USER, userService.getById(id));

        return PAGE_USERS;
    }

    @PostMapping("/add")
    public String addUser(@ModelAttribute(ATTRIBUTE_USER_FORM) User user) {
        logger.info("Add new User: " + user);

        if (user.getId() == null) {
            user.setRole(new Role(RoleType.USER));
            user.setStatus(new UserStatus(UserStatusType.ACTIVE));

            userService.save(user);
        } else {
            userService.update(user);
        }

        return "redirect:" + MAPPING_USERS;
    }

    @GetMapping("/remove/{id}")
    public String removeUser(@PathVariable("id") long id) {
        logger.info("Remove User with id: " + id);

        userService.delete(id);

        return "redirect:" + MAPPING_USERS;
    }

    @GetMapping("/edit/{id}")
    public String editUser(@PathVariable("id") long id, Model model) {
        logger.info("Edit User with id: " + id);

        User user = userService.getById(id);
        model.addAttribute(ATTRIBUTE_USER_FORM, user);
        model.addAttribute(ATTRIBUTE_USER_LIST, userService.getAll());

        return PAGE_USERS;
    }
}
