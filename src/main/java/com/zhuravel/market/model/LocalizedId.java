package com.zhuravel.market.model;

import org.springframework.context.i18n.LocaleContextHolder;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

/**
 * @author Evgenii Zhuravel created on 28.05.2022
 * @version 1.0
 */
@Embeddable
public class LocalizedId implements Serializable {

    private Long id;

    private String locale;

    public LocalizedId() {
        this.locale = LocaleContextHolder.getLocale().getLanguage();
    }

    public LocalizedId(String locale) {
        this.locale = locale;
    }

    public LocalizedId(Long id) {
        this.id = id;
        this.locale = LocaleContextHolder.getLocale().getLanguage();
    }

    public LocalizedId(Long id, String locale) {
        this.id = id;
        this.locale = locale;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LocalizedId that = (LocalizedId) o;
        return id.equals(that.id) && locale.equals(that.locale);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, locale);
    }

    @Override
    public String toString() {
        return "LocalizedId{" +
                "id=" + id +
                ", locale='" + locale + '\'' +
                '}';
    }
}
