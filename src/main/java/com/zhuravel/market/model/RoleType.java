package com.zhuravel.market.model;

/**
 * @author Evgenii Zhuravel created on 22.05.2022
 * @version 1.0
 */
public enum RoleType {
    ADMIN("ROLE_ADMIN"),
    SELLER("ROLE_SELLER"),
    USER("ROLE_USER");

    RoleType(String role_user) {}
}
