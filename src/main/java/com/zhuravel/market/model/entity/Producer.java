package com.zhuravel.market.model.entity;

import org.hibernate.annotations.Immutable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Evgenii Zhuravel created on 25.05.2022
 * @version 1.0
 */
@Entity
@Immutable
@Table(name = "producer")
public class Producer implements Comparable<Producer> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int compareTo(Producer o) {
        return name.compareTo(o.name);
    }
}