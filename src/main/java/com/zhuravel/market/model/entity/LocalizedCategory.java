package com.zhuravel.market.model.entity;

import com.zhuravel.market.model.LocalizedId;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

/**
 * @author Evgenii Zhuravel created on 19.05.2022
 * @version 1.0
 */
@Entity
@Table(name = "category_translation")
public class LocalizedCategory {
    @EmbeddedId
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private LocalizedId localizedId;

    @Column(table = "category_translation", name = "localized_name")
    private String localizedName;

    @ManyToOne
    @MapsId("id")
    @JoinColumn(name = "id")
    private Category category;

    public LocalizedId getLocalizedId() {
        return localizedId;
    }

    public void setLocalizedId(LocalizedId id) {
        this.localizedId = id;
    }

    public String getLocalizedName() {
        return localizedName;
    }

    public void setLocalizedName(String localizedName) {
        this.localizedName = localizedName;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}
