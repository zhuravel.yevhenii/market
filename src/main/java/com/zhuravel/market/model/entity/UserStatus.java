package com.zhuravel.market.model.entity;

import com.zhuravel.market.model.UserStatusType;
import org.hibernate.annotations.Immutable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Evgenii Zhuravel created on 24.05.2022
 * @version 1.0
 */
@Entity
@Immutable
@Table(name = "user_status")
public class UserStatus {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UserStatus() {
    }

    public UserStatus(UserStatusType statusType) {
        this.id = statusType.ordinal() + 1L;
        this.name = statusType.name();
    }

    @Override
    public String toString() {
        return "UserStatusType{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
