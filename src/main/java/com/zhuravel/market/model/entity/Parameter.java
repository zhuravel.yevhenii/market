package com.zhuravel.market.model.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Objects;

/**
 * @author Evgenii Zhuravel created on 29.05.2022
 * @version 1.0
 */
@Entity
@Table(name = "product_parameter")
public class Parameter implements Comparable<Parameter> {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "parameter_type_id")
    private ParameterType type;

    private String sense;

    @ManyToOne
    @JoinTable(name = "product_product_parameter",
            joinColumns = {@JoinColumn(name = "parameter_id")},
            inverseJoinColumns = {@JoinColumn(name = "product_id")}
    )
    //@JoinColumn(name = "product_id")
    private Product product;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ParameterType getType() {
        return type;
    }

    public void setType(ParameterType type) {
        this.type = type;
    }

    public String getSense() {
        return sense;
    }

    public void setSense(String sense) {
        this.sense = sense;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Parameter parameter = (Parameter) o;
        return type.equals(parameter.type) && sense.equals(parameter.sense);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, sense);
    }

    @Override
    public int compareTo(Parameter o) {
        return sense.compareTo(o.sense);
    }
}
