package com.zhuravel.market.model.entity;

import com.zhuravel.market.model.LocalizedId;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * @author Evgenii Zhuravel created on 29.05.2022
 * @version 1.0
 */
@Entity
@Table(name = "product_parameter_type_translation")
public class LocalizedParameterType implements Comparable<LocalizedParameterType> {
    @EmbeddedId
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private LocalizedId localizedId;

    @Column(name = "localized_name")
    private String localizedName;

    @OneToOne
    @MapsId("id")
    @JoinColumn(name = "id")
    private ParameterType parameterType;

    public LocalizedId getLocalizedId() {
        return localizedId;
    }

    public void setLocalizedId(LocalizedId id) {
        this.localizedId = id;
    }

    public ParameterType getParameterType() {
        return parameterType;
    }

    public void setParameterType(ParameterType parameterType) {
        this.parameterType = parameterType;
    }

    public String getLocalizedName() {
        return localizedName;
    }

    public void setLocalizedName(String localizedName) {
        this.localizedName = localizedName;
    }

    @Override
    public int compareTo(LocalizedParameterType o) {
        return localizedName.compareTo(o.localizedName);
    }
}