package com.zhuravel.market.utils;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.TimeZone;

/**
 * @author Evgenii Zhuravel created on 06.06.2022
 * @version 1.0
 */
public class DateTimeConverter {

    public static void main(String[] args) {
        System.out.println(getMilliseconds(LocalDateTime.now()));
    }

    public static String getFormattedDateTime(Long milliseconds) {
        LocalDateTime localDateTime = LocalDateTime.ofInstant(Instant.ofEpochSecond(milliseconds), TimeZone.getDefault().toZoneId());

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

        return localDateTime.format(formatter);
    }

    public static Long getMilliseconds(LocalDateTime localDateTime) {
        ZonedDateTime zonedDateTime = ZonedDateTime.of(localDateTime, TimeZone.getDefault().toZoneId());
        return zonedDateTime.toEpochSecond();
    }
}
