package com.zhuravel.market.utils;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * @author Evgenii Zhuravel created on 25.05.2022
 * @version 1.0
 */
public class EncryptedPasswordUtils {

    public static void main(String[] args) {
        String password = "Admin";

        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

        System.out.println("Encrypted Password: " + bCryptPasswordEncoder.encode(password));

        System.out.println("Encrypted Seller Password: " + bCryptPasswordEncoder.encode("Seller"));
        System.out.println("Encrypted User Password: " + bCryptPasswordEncoder.encode("User"));
        System.out.println("Encrypted ActiveUser Password: " + bCryptPasswordEncoder.encode("ActiveUser"));
        System.out.println("Encrypted BlockedUser Password: " + bCryptPasswordEncoder.encode("BlockedUser"));
        System.out.println("Encrypted DeletedUser Password: " + bCryptPasswordEncoder.encode("DeletedUser"));

    }
}
