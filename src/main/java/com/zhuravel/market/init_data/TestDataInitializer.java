package com.zhuravel.market.init_data;

import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.Location;
import org.flywaydb.core.api.configuration.ClassicConfiguration;
import org.flywaydb.core.api.configuration.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * @author Evgenii Zhuravel
 * @version 1.0
 */
@Component
@Order(1)
public class TestDataInitializer implements CommandLineRunner {
    private static final Logger logger = LoggerFactory.getLogger(TestDataInitializer.class);

    private final String dataSourceUrl;
    private final String dataSourceUserName;
    private final String dataSourcePassword;
    private final String testDataScriptLocation;
    private final boolean enable;

    public TestDataInitializer(
            @Value("${spring.datasource.url}") String dataSourceUrl,
            @Value("${spring.datasource.username}") String dataSourceUserName,
            @Value("${spring.datasource.password}") String dataSourcePassword,
            @Value("${test.data.script-location}") String testDataScriptLocation,
            @Value("${test.data.enabled}") boolean enable) {
        this.dataSourceUrl = dataSourceUrl;
        this.dataSourceUserName = dataSourceUserName;
        this.dataSourcePassword = dataSourcePassword;
        this.testDataScriptLocation = testDataScriptLocation;
        this.enable = enable;
    }

    @Override
    public void run(String... args) {
        if (enable) {
            Configuration configuration = buildFlyWayConfiguration();

            Flyway flyway = new Flyway(configuration);
            flyway.migrate();

            logger.debug("In run - Database has been populated with test data successfully");
        }
    }

    private Configuration buildFlyWayConfiguration() {
        ClassicConfiguration configuration = new ClassicConfiguration();

        configuration.setDataSource(dataSourceUrl, dataSourceUserName, dataSourcePassword);
        configuration.setLocations(new Location(testDataScriptLocation));
        configuration.setValidateOnMigrate(false);

        return configuration;
    }
}
