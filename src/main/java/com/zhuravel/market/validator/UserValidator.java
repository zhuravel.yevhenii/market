package com.zhuravel.market.validator;

import com.zhuravel.market.model.entity.User;
import com.zhuravel.market.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * @author Evgenii Zhuravel created on 22.05.2022
 * @version 1.0
 */
@Component
public class UserValidator implements Validator {
    private static final Logger logger = LoggerFactory.getLogger(UserValidator.class);

    private UserService userService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return User.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        logger.debug("User validating begin");

        User user = (User) target;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "login", "required");

        if (user.getLogin().length() < 5 || user.getLogin().length() > 32) {
            errors.rejectValue("login", "userForm.login.size");
        }

        userService.getByLogin(user.getLogin())
                .ifPresent(oldUser -> errors.rejectValue("login", "userForm.login.duplicate"));

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "required");

        if (user.getPassword().length() < 5) {
            errors.rejectValue("password", "userForm.password.size");
        }
    }
}
