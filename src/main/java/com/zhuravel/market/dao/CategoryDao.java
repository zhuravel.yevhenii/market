package com.zhuravel.market.dao;

import com.zhuravel.market.model.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author Evgenii Zhuravel created on 20.05.2022
 * @version 1.0
 */
public interface CategoryDao extends JpaRepository<Category, Long> {

    List<Category> findAllByParentCategoryId(Long parentCategoryId);
}
