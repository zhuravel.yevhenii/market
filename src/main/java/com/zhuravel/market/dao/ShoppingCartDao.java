package com.zhuravel.market.dao;

import com.zhuravel.market.model.entity.ShoppingCart;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Evgenii Zhuravel created on 20.05.2022
 * @version 1.0
 */
public interface ShoppingCartDao extends JpaRepository<ShoppingCart, Long> {

    void deleteAllByUserIdAndProductId(Long userId, Long productId);
}
