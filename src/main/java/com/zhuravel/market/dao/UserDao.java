package com.zhuravel.market.dao;

import com.zhuravel.market.model.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * @author Evgenii Zhuravel created on 20.05.2022
 * @version 1.0
 */
public interface UserDao extends JpaRepository<User, Long> {
    Optional<User> findByLogin(String login);
}
