package com.zhuravel.market.dao;

import com.zhuravel.market.model.LocalizedId;
import com.zhuravel.market.model.entity.LocalizedCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * @author Evgenii Zhuravel created on 20.05.2022
 * @version 1.0
 */
public interface LocalizedCategoryDao extends JpaRepository<LocalizedCategory, LocalizedId> {

    /*Optional<Category> findByIdAndLocale(@Param("id") Long id, @Param("locale") String locale);*/

    @Query("SELECT lc FROM LocalizedCategory lc WHERE lc.category.parentCategoryId = :id AND lc.localizedId.locale = :locale")
    List<LocalizedCategory> findAllByParentCategoryIdAndLocale(@Param("id") Long id, @Param("locale") String locale);
}
