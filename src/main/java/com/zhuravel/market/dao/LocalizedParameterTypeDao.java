package com.zhuravel.market.dao;

import com.zhuravel.market.model.LocalizedId;
import com.zhuravel.market.model.entity.LocalizedParameterType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

/**
 * @author Evgenii Zhuravel created on 20.05.2022
 * @version 1.0
 */
public interface LocalizedParameterTypeDao extends JpaRepository<LocalizedParameterType, LocalizedId> {

    @Query("SELECT lpt FROM LocalizedParameterType lpt WHERE lpt.parameterType.id = :id AND lpt.localizedId.locale = :locale")
    Optional<LocalizedParameterType> findById(@Param("id") Long id, @Param("locale") String locale);

    @Query("SELECT lpt FROM LocalizedParameterType lpt WHERE lpt.localizedId.locale = :locale")
    List<LocalizedParameterType> findAll(@Param("locale") String locale);

    @Query("SELECT lpt FROM LocalizedParameterType lpt JOIN ParameterType pt ON lpt.parameterType.id = pt.id WHERE pt.category.id = :id AND lpt.localizedId.locale = :locale")
    List<LocalizedParameterType> findAllByCategoryId(@Param("id") Long id, @Param("locale") String locale);
}
