package com.zhuravel.market.service.exception;

/**
 * @author Evgenii Zhuravel created on 27.05.2022
 * @version 1.0
 */
public class ResourceNotFoundException extends RuntimeException {
    private final Class<?> resourceClass;

    public ResourceNotFoundException(String message) {
        super(message);
        this.resourceClass = null;
    }

    public ResourceNotFoundException(String message, Class<?> resourceClass) {
        super(message);
        this.resourceClass = resourceClass;
    }

    public Class<?> getResourceClass() {
        return resourceClass;
    }
}
