package com.zhuravel.market.service;

import com.zhuravel.market.model.entity.Category;

import java.util.List;

/**
 * @author Evgenii Zhuravel created on 19.05.2022
 * @version 1.0
 */
public interface CategoryService extends BaseService<Category, Long> {

    Category getById(Long id);
    List<Category> getSubCategories(Long id);
}
