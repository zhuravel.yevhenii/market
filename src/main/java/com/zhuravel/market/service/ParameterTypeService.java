package com.zhuravel.market.service;

import com.zhuravel.market.model.entity.LocalizedParameterType;

import java.util.List;

/**
 * @author Evgenii Zhuravel created on 19.05.2022
 * @version 1.0
 */
public interface ParameterTypeService extends BaseService<LocalizedParameterType, Long> {
    List<LocalizedParameterType> getAllByCategoryId(Long id);
}
