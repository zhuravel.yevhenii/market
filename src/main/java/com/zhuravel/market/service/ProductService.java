package com.zhuravel.market.service;

import com.zhuravel.market.model.entity.Product;
import com.zhuravel.market.service.dto.ProductsCatalogDto;
import com.zhuravel.market.service.filter_criteria.ParameterSearchCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * @author Evgenii Zhuravel created on 19.05.2022
 * @version 1.0
 */
public interface ProductService extends BaseService<Product, Long> {

    List<Product> getAllByProducerName(String name);

    ProductsCatalogDto getAll(ParameterSearchCriteria criteria, Pageable pageable);
}
