package com.zhuravel.market.service;

import com.zhuravel.market.model.entity.Parameter;

/**
 * @author Evgenii Zhuravel created on 19.05.2022
 * @version 1.0
 */
public interface ParameterService extends BaseService<Parameter, Long> {

}
