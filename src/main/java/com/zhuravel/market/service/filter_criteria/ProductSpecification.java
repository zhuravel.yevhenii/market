package com.zhuravel.market.service.filter_criteria;

import com.zhuravel.market.model.entity.Product;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Evgenii Zhuravel created on 01.06.2022
 * @version 1.0
 */
public class ProductSpecification implements Specification<Product> {

    private static final String FIELD_CATEGORIES_ID = "id";
    public static final String FIELD_PARAMETERS = "parameters";
    public static final String FIELD_PRODUCER = "producer";
    public static final String FIELD_CATEGORY = "category";

    private final ParameterSearchCriteria parametersCriteria;

    public ProductSpecification(ParameterSearchCriteria parametersCriteria) {
        this.parametersCriteria = parametersCriteria;
    }

    @Override
    public Predicate toPredicate(Root<Product> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        query.distinct(true);

        Predicate[] predicates = buildPredicates(root, criteriaBuilder);

        return criteriaBuilder.and(predicates);
    }

    private Predicate[] buildPredicates(Root<Product> root, CriteriaBuilder criteriaBuilder) {
        List<Predicate> predicates = new ArrayList<>();

        predicates.add(createCategoryPredicate(root, criteriaBuilder, parametersCriteria.getCategoryId()));

        parametersCriteria.getProducers().ifPresent(producers -> {
            Predicate predicate = createProducerPredicate(root, criteriaBuilder, producers);
            if (predicate != null) {
                predicates.add(predicate);
            }
        });

        for (ParameterSearchCriteria.ParametersType parametersType : parametersCriteria.getParameterTypes()) {
            if (parametersType.getParameters().size() > 0) {
                Predicate predicate = createParameterPredicate(root, criteriaBuilder, parametersType);
                if (predicate != null) {
                    predicates.add(predicate);
                }
            }
        }

        return predicates.toArray(new Predicate[0]);
    }

    private Predicate createProducerPredicate(Root<Product> root, CriteriaBuilder cb, ParameterSearchCriteria.ParametersType producers) {
        List<Predicate> parameterPredicates = new ArrayList<>();
        for (ParameterSearchCriteria.SelectedParameter producer : producers.getParameters()) {
            parameterPredicates.add(addProducerPredicate(root, cb, producer.getId()));
        }
        if (parameterPredicates.size() > 0) {
            return cb.or(parameterPredicates.toArray(Predicate[]::new));
        }
        return null;
    }

    private Predicate addProducerPredicate(Root<Product> root, CriteriaBuilder cb, Long producerId) {
        return cb.equal(root.get(FIELD_PRODUCER), producerId);
    }

    private Predicate createParameterPredicate(Root<Product> root, CriteriaBuilder cb, ParameterSearchCriteria.ParametersType parametersType) {
        List<Predicate> parameterPredicates = new ArrayList<>();
        for (ParameterSearchCriteria.SelectedParameter parameter : parametersType.getParameters()) {
            parameterPredicates.add(addParameterPredicate(root, cb, parameter.getId()));
        }
        if (parameterPredicates.size() > 0) {
            return cb.or(parameterPredicates.toArray(Predicate[]::new));
        }
        return null;
    }

    private Predicate addParameterPredicate(Root<Product> root, CriteriaBuilder cb, Long parameterId) {
        final Join<Object, Object> parametersJoin = root.join(FIELD_PARAMETERS);
        return cb.equal(parametersJoin.get(FIELD_CATEGORIES_ID), parameterId);
    }


    private Predicate createCategoryPredicate(Root<Product> root, CriteriaBuilder cb, Long categoryId) {
        return cb.equal(root.get(FIELD_CATEGORY), categoryId);
    }
}
