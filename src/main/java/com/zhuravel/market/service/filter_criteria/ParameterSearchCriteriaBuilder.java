package com.zhuravel.market.service.filter_criteria;

import com.zhuravel.market.service.dto.SelectedParametersDto;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Evgenii Zhuravel created on 02.06.2022
 * @version 1.0
 */
public class ParameterSearchCriteriaBuilder {

    public static ParameterSearchCriteria build(Long categoryId, SelectedParametersDto selectedParameters) {
        ParameterSearchCriteria parameterSearchCriteria = new ParameterSearchCriteria();

        parameterSearchCriteria.setCategoryId(categoryId);
        if (selectedParameters.getProducers() != null) {
            parameterSearchCriteria.setProducers(extractSelectedProducers(selectedParameters.getProducers()));
        }
        parameterSearchCriteria.setParameterTypes(extractSelectedParameters(selectedParameters.getParameterTypes()));

        return parameterSearchCriteria;
    }

    private static ParameterSearchCriteria.ParametersType extractSelectedProducers(SelectedParametersDto.ParametersType producers) {
        return getParametersType(producers);
    }

    private static List<ParameterSearchCriteria.ParametersType> extractSelectedParameters(List<SelectedParametersDto.ParametersType> selectedTypes) {
        List<ParameterSearchCriteria.ParametersType> parametersTypes = new ArrayList<>();

        for (SelectedParametersDto.ParametersType selectedType: selectedTypes) {
            ParameterSearchCriteria.ParametersType parametersType = getParametersType(selectedType);

            parametersTypes.add(parametersType);
        }

        return parametersTypes;
    }

    private static ParameterSearchCriteria.ParametersType getParametersType(SelectedParametersDto.ParametersType selectedType) {
        ParameterSearchCriteria.ParametersType parametersType = new ParameterSearchCriteria.ParametersType();
        parametersType.setName(selectedType.getName());

        List<ParameterSearchCriteria.SelectedParameter> parameters = new ArrayList<>();

        for (SelectedParametersDto.SelectedParameter selectedParameter : selectedType.getParameters()) {
            if (selectedParameter.isSelected()) {
                parameters.add(new ParameterSearchCriteria.SelectedParameter(selectedParameter.getId(), selectedParameter.getName(), selectedParameter.isSelected()));
            }
        }
        parametersType.setParameters(parameters);
        return parametersType;
    }
}
