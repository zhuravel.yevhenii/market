package com.zhuravel.market.service.impl;

import com.zhuravel.market.dao.ProductDao;
import com.zhuravel.market.dao.ShoppingCartDao;
import com.zhuravel.market.dao.UserDao;
import com.zhuravel.market.model.LocaleType;
import com.zhuravel.market.model.entity.Product;
import com.zhuravel.market.model.entity.ShoppingCart;
import com.zhuravel.market.model.entity.User;
import com.zhuravel.market.service.UserService;
import com.zhuravel.market.service.dto.ShoppingCartDto;
import com.zhuravel.market.service.dto.ShoppingCartDtoBuilder;
import com.zhuravel.market.service.exception.ResourceNotFoundException;
import com.zhuravel.market.utils.DateTimeConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.lang.String.format;

/**
 * @author Evgenii Zhuravel created on 19.05.2022
 * @version 1.0
 */
@Service
public class UserServiceImpl implements UserService {
    private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    private final UserDao userDao;
    private final ShoppingCartDao shoppingCartDao;
    private final ProductDao productDao;

    @Autowired
    public UserServiceImpl(UserDao userDao, ShoppingCartDao shoppingCartDao, ProductDao productDao) {
        this.userDao = userDao;
        this.shoppingCartDao = shoppingCartDao;
        this.productDao = productDao;
    }

    @Override
    @Transactional
    public void save(User user) {
        userDao.save(user);

        logger.debug("Successfully saved user: {}", user);
    }

    @Override
    @Transactional
    public void update(User user) {
        userDao.save(user);

        logger.debug("Successfully updated user: {}", user);
    }

    @Override
    @Transactional
    public void delete(Long id) {
        userDao.deleteById(id);
    }

    @Override
    @Transactional
    public User getById(Long id) {
        return userDao.findById(id).orElseThrow(() ->
                new ResourceNotFoundException(format("User with %s not found", id), User.class));
    }

    @Override
    @Transactional
    public List<User> getAll() {
        return userDao.findAll();
    }

    @Override
    public Optional<User> getByLogin(String login) {
        return userDao.findByLogin(login);
    }

    @Override
    public void updateUserLocale(Long userId, LocaleType locale) {
        User user = getById(userId);

        user.setLocale(locale);

        update(user);
        logger.debug("In updateUserLocale - Successfully updated locale for user with id:[{}] to [{}]", userId, locale);
    }

    @Override
    public ShoppingCartDto getShoppingCart(String login) {
        User user = userDao.findByLogin(login).orElseThrow(() -> new ResourceNotFoundException(format("User with login %s not found", login), User.class));

        return ShoppingCartDtoBuilder.build(user.getId(), user.getProducts());
    }

    @Override
    public void updateShoppingCart(ShoppingCartDto shoppingCartDto) {
        User user = userDao.findById(shoppingCartDto.getUserId()).orElseThrow(() ->
                new ResourceNotFoundException(format("User with %s not found", shoppingCartDto.getUserId()), User.class));

        Map<Long, Integer> quantities = shoppingCartDto.getSelectedProducts().getQuantities();

        List<ShoppingCart> products = user.getProducts().stream()
                .peek(shoppingCart -> {
                    if (quantities.containsKey(shoppingCart.getProduct().getId())) {
                        applyQuantity(quantities, shoppingCart);
                    }
                })
                .filter(shoppingCart -> shoppingCart.getQuantity() > 0)
                .collect(Collectors.toList());

        user.setProducts(products);
        userDao.save(user);
    }

    @Override
    public void checkout(ShoppingCartDto shoppingCartDto) {
        User user = userDao.findById(shoppingCartDto.getUserId()).orElseThrow(() ->
                new ResourceNotFoundException(format("User with %s not found", shoppingCartDto.getUserId()), User.class));

        Map<Long, Integer> quantities = shoppingCartDto.getSelectedProducts().getQuantities();

        Long currentTime = DateTimeConverter.getMilliseconds(LocalDateTime.now());

        List<ShoppingCart> products = user.getProducts().stream()
                .peek(shoppingCart -> {
                    if (quantities.containsKey(shoppingCart.getProduct().getId())) {
                        applyQuantity(quantities, shoppingCart);

                        shoppingCart.setRegistrationDateTime(currentTime);
                    }
                })
                .filter(shoppingCart -> shoppingCart.getQuantity() > 0)
                .collect(Collectors.toList());

        user.setProducts(products);
        userDao.save(user);
    }

    @Override
    public void add(String login, Long productId) {
        ShoppingCart shoppingCart = new ShoppingCart();
        User user = userDao.findByLogin(login).orElseThrow(() ->
                new ResourceNotFoundException(format("User with login '%s' not found", login), User.class));
        Product product = productDao.findById(productId).orElseThrow(() ->
                new ResourceNotFoundException(format("Product with id %s not found", productId), Product.class));

        shoppingCart.setUser(user);
        shoppingCart.setProduct(product);
        shoppingCart.setQuantity(1);

        shoppingCartDao.saveAndFlush(shoppingCart);
    }

    private void applyQuantity(Map<Long, Integer> quantities, ShoppingCart shoppingCart) {
        Integer quantity = quantities.get(shoppingCart.getProduct().getId());
        shoppingCart.setQuantity(quantity);

        if (quantity < 1) {
            shoppingCartDao.delete(shoppingCart);
        }
    }
}
