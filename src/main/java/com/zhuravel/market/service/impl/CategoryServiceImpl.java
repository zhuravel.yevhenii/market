package com.zhuravel.market.service.impl;

import com.zhuravel.market.dao.CategoryDao;
import com.zhuravel.market.model.LocalizedId;
import com.zhuravel.market.model.entity.Category;
import com.zhuravel.market.service.CategoryService;
import com.zhuravel.market.service.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

import static java.lang.String.format;

/**
 * @author Evgenii Zhuravel created on 19.05.2022
 * @version 1.0
 */
@Service
public class CategoryServiceImpl implements CategoryService {

    @Value("${default.category.image.path}")
    private String defaultImagePath;


    private CategoryDao categoryDao;

    @Autowired
    public void setCategoryDao(CategoryDao categoryDao) {
        this.categoryDao = categoryDao;
    }

    @Override
    @Transactional
    public void save(Category category) {
        categoryDao.save(category);
    }

    @Override
    @Transactional
    public void update(Category category) {
        categoryDao.save(category);
    }

    @Override
    @Transactional
    public void delete(Long id) {
        categoryDao.deleteById(id);
    }

    @Override
    @Transactional
    public Category getById(Long id) {
        return categoryDao.findById(id).orElseThrow(() ->
                new ResourceNotFoundException(format("Category with %s not found", id), Category.class));
    }

    @Override
    @Transactional
    public List<Category> getAll() {
        return categoryDao.findAll();
    }

    @Override
    public List<Category> getSubCategories(Long id) {
        LocalizedId localizedId = new LocalizedId(id);
        List<Category> localizedCategories = categoryDao.findAllByParentCategoryId(localizedId.getId());

        localizedCategories = setDefaultIcon(localizedCategories);

        return localizedCategories;
    }

    private List<Category> setDefaultIcon(List<Category> categories) {
        return categories.stream()
                .peek(category -> {
                    if (category.getImagePath() == null) {
                        category.setImagePath(defaultImagePath);
                    }
                })
                .collect(Collectors.toList());
    }
}
