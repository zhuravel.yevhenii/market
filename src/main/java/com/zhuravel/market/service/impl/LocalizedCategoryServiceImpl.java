package com.zhuravel.market.service.impl;

import com.zhuravel.market.dao.LocalizedCategoryDao;
import com.zhuravel.market.model.LocalizedId;
import com.zhuravel.market.model.entity.Category;
import com.zhuravel.market.model.entity.LocalizedCategory;
import com.zhuravel.market.service.LocalizedCategoryService;
import com.zhuravel.market.service.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

import static java.lang.String.format;

/**
 * @author Evgenii Zhuravel created on 19.05.2022
 * @version 1.0
 */
@Service
public class LocalizedCategoryServiceImpl implements LocalizedCategoryService {

    @Value("${default.category.image.path}")
    private String defaultImagePath;

    private LocalizedCategoryDao localizedCategoryDao;

    @Autowired
    public void setCategoryDao(LocalizedCategoryDao localizedCategoryDao) {
        this.localizedCategoryDao = localizedCategoryDao;
    }

    @Override
    @Transactional
    public void save(LocalizedCategory category) {
        localizedCategoryDao.save(category);
    }

    @Override
    @Transactional
    public void update(LocalizedCategory category) {
        localizedCategoryDao.save(category);
    }

    @Override
    @Transactional
    public void delete(Long id) {
        localizedCategoryDao.deleteById(new LocalizedId(id));
    }

    @Override
    @Transactional
    public LocalizedCategory getById(Long id) {
        LocalizedId localizedId = new LocalizedId(id);

        return localizedCategoryDao.findById(localizedId).orElseThrow(() ->
                new ResourceNotFoundException(format("Category with %s not found", id), Category.class));
    }

    @Override
    @Transactional
    public List<LocalizedCategory> getAll() {
        return localizedCategoryDao.findAll();
    }

    @Override
    public List<LocalizedCategory> getSubCategories(Long id) {
        LocalizedId localizedId = new LocalizedId(id);
        List<LocalizedCategory> localizedCategories = localizedCategoryDao.findAllByParentCategoryIdAndLocale(localizedId.getId(), localizedId.getLocale());

        localizedCategories = setDefaultIcon(localizedCategories);

        return localizedCategories;
    }

    private List<LocalizedCategory> setDefaultIcon(List<LocalizedCategory> localizedCategories) {
        return localizedCategories.stream()
                .peek(localizedCategory -> {
                    if (localizedCategory.getCategory().getImagePath() == null) {
                        localizedCategory.getCategory().setImagePath(defaultImagePath);
                    }
                })
                .collect(Collectors.toList());
    }
}
