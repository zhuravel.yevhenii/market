package com.zhuravel.market.service.impl;

import com.zhuravel.market.dao.LocalizedParameterTypeDao;
import com.zhuravel.market.model.LocalizedId;
import com.zhuravel.market.model.entity.LocalizedParameterType;
import com.zhuravel.market.model.entity.ParameterType;
import com.zhuravel.market.service.ParameterTypeService;
import com.zhuravel.market.service.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

import static java.lang.String.format;

/**
 * @author Evgenii Zhuravel created on 19.05.2022
 * @version 1.0
 */
@Service
public class ParameterTypeServiceImpl implements ParameterTypeService {

    private LocalizedParameterTypeDao parameterTypeDao;

    @Autowired
    public void setParameterTypeDao(LocalizedParameterTypeDao parameterTypeDao) {
        this.parameterTypeDao = parameterTypeDao;
    }

    @Override
    @Transactional
    public void save(LocalizedParameterType category) {
        parameterTypeDao.save(category);
    }

    @Override
    @Transactional
    public void update(LocalizedParameterType category) {
        parameterTypeDao.save(category);
    }

    @Override
    @Transactional
    public void delete(Long id) {
        parameterTypeDao.deleteById(new LocalizedId(id));
    }

    @Override
    @Transactional
    public LocalizedParameterType getById(Long id) {
        return parameterTypeDao.findById(new LocalizedId(id)).orElseThrow(() ->
                new ResourceNotFoundException(format("ParameterType with %s not found", id), ParameterType.class));
    }

    @Override
    @Transactional
    public List<LocalizedParameterType> getAll() {
        return parameterTypeDao.findAll();
    }

    @Override
    public List<LocalizedParameterType> getAllByCategoryId(Long id) {
        LocalizedId localizedId = new LocalizedId(id);
        return parameterTypeDao.findAllByCategoryId(id, localizedId.getLocale());
    }
}
