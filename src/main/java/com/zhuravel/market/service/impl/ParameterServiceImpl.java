package com.zhuravel.market.service.impl;

import com.zhuravel.market.dao.ParameterDao;
import com.zhuravel.market.model.entity.Parameter;
import com.zhuravel.market.service.ParameterService;
import com.zhuravel.market.service.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

import static java.lang.String.format;

/**
 * @author Evgenii Zhuravel created on 19.05.2022
 * @version 1.0
 */
@Service
public class ParameterServiceImpl implements ParameterService {

    private ParameterDao parameterDao;

    @Autowired
    public void setParameterDao(ParameterDao parameterDao) {
        this.parameterDao = parameterDao;
    }

    @Override
    @Transactional
    public void save(Parameter category) {
        parameterDao.save(category);
    }

    @Override
    @Transactional
    public void update(Parameter category) {
        parameterDao.save(category);
    }

    @Override
    @Transactional
    public void delete(Long id) {
        parameterDao.deleteById(id);
    }

    @Override
    @Transactional
    public Parameter getById(Long id) {
        return parameterDao.findById(id).orElseThrow(() ->
                new ResourceNotFoundException(format("Parameter with %s not found", id), Parameter.class));
    }

    @Override
    @Transactional
    public List<Parameter> getAll() {
        return parameterDao.findAll();
    }
}
