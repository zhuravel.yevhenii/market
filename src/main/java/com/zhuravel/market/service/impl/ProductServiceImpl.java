package com.zhuravel.market.service.impl;

import com.zhuravel.market.dao.ProductDao;
import com.zhuravel.market.model.entity.Product;
import com.zhuravel.market.service.ProductService;
import com.zhuravel.market.service.dto.ProductsCatalogDto;
import com.zhuravel.market.service.exception.ResourceNotFoundException;
import com.zhuravel.market.service.filter_criteria.ParameterSearchCriteria;
import com.zhuravel.market.service.filter_criteria.ProductSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

import static java.lang.String.format;

/**
 * @author Evgenii Zhuravel created on 19.05.2022
 * @version 1.0
 */
@Service
public class ProductServiceImpl implements ProductService {

    private ProductDao productDao;

    @Autowired
    public void setProductDao(ProductDao productDao) {
        this.productDao = productDao;
    }

    @Override
    @Transactional
    public void save(Product product) {
        productDao.save(product);
    }

    @Override
    @Transactional
    public void update(Product product) {
        productDao.save(product);
    }

    @Override
    @Transactional
    public void delete(Long id) {
        productDao.deleteById(id);
    }

    @Override
    @Transactional
    public Product getById(Long id) {
        return productDao.findById(id).orElseThrow(() ->
                new ResourceNotFoundException(format("Product with %s not found", id), Product.class));
    }

    @Override
    @Transactional
    public List<Product> getAll() {
        return productDao.findAll();
    }

    @Override
    public List<Product> getAllByProducerName(String name) {
        return productDao.findAllByProducerName(name);
    }

    @Override
    public ProductsCatalogDto getAll(ParameterSearchCriteria criteria, Pageable pageable) {
        Specification<Product> productSpecification = new ProductSpecification(criteria);

        Page<Product> products = productDao.findAll(productSpecification, pageable);

        ProductsCatalogDto productsCatalogDTO = new ProductsCatalogDto();
        productsCatalogDTO.setProducts(products);
        productsCatalogDTO.setProducers(products.getContent().stream().map(Product::getProducer).collect(Collectors.toSet()));

        return productsCatalogDTO;
    }
}
