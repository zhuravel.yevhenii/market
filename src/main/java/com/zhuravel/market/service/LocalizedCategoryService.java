package com.zhuravel.market.service;

import com.zhuravel.market.model.entity.LocalizedCategory;

import java.util.List;

/**
 * @author Evgenii Zhuravel created on 19.05.2022
 * @version 1.0
 */
public interface LocalizedCategoryService extends BaseService<LocalizedCategory, Long> {

    LocalizedCategory getById(Long id);
    List<LocalizedCategory> getSubCategories(Long id);
}
