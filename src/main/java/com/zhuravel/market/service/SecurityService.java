package com.zhuravel.market.service;

/**
 * @author Evgenii Zhuravel created on 22.05.2022
 * @version 1.0
 */
public interface SecurityService {

    String findLoggedInLogin();

    void autoLogin(String login, String password);
}
