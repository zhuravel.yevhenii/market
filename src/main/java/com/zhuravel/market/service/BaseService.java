package com.zhuravel.market.service;

import java.util.List;

/**
 * @author Evgenii Zhuravel created on 19.05.2022
 * @version 1.0
 */
public interface BaseService<E, ID> {
    void save(E e);

    void update(E e);

    void delete(ID id);

    E getById(ID id);

    List<E> getAll();
}
