package com.zhuravel.market.service;

import com.zhuravel.market.model.LocaleType;
import com.zhuravel.market.model.entity.User;
import com.zhuravel.market.service.dto.ShoppingCartDto;

import java.util.Optional;

/**
 * @author Evgenii Zhuravel created on 19.05.2022
 * @version 1.0
 */
public interface UserService extends BaseService<User, Long> {

    Optional<User> getByLogin(String login);

    void updateUserLocale(Long userId, LocaleType locale);

    ShoppingCartDto getShoppingCart(String login);

    void updateShoppingCart(ShoppingCartDto shoppingCartDto);

    void checkout(ShoppingCartDto shoppingCartDto);

    void add(String login, Long productId);
}
