package com.zhuravel.market.service.dto;

import com.zhuravel.market.model.entity.ShoppingCart;
import com.zhuravel.market.utils.DateTimeConverter;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

/**
 * @author Evgenii Zhuravel created on 06.06.2022
 * @version 1.0
 */
public class ShoppingCartDtoBuilder {

    public static ShoppingCartDto build(Long userId, List<ShoppingCart> products) {
        ShoppingCartDto shoppingCartDto = new ShoppingCartDto();

        Map<ProductDto, Integer> newProducts = new TreeMap<>();
        Map<String, Map<ProductDto, Integer>> checkouts = new TreeMap<>();

        for(ShoppingCart cart: products) {
            if (cart.getRegistrationDateTime() == null) {
                newProducts.put(ProductDtoConverter.convert(cart.getProduct()), cart.getQuantity());
            } else {
                String formattedDateTime = DateTimeConverter.getFormattedDateTime(cart.getRegistrationDateTime());

                if (checkouts.get(formattedDateTime) == null) {
                    Map<ProductDto, Integer> p = new TreeMap<>();

                    p.put(ProductDtoConverter.convert(cart.getProduct()), cart.getQuantity());

                    checkouts.put(formattedDateTime, p);
                } else {
                    checkouts.get(formattedDateTime).put(ProductDtoConverter.convert(cart.getProduct()), cart.getQuantity());
                }
            }
        }

        shoppingCartDto.setUserId(userId);
        shoppingCartDto.setSelectedProducts(initForm(newProducts));
        shoppingCartDto.setProducts(newProducts);
        shoppingCartDto.setCheckouts(checkouts);

        return shoppingCartDto;
    }

    private static SelectedProductsDto initForm(Map<ProductDto, Integer> products) {
        SelectedProductsDto form = new SelectedProductsDto();

        form.setQuantities(products.entrySet().stream()
                .collect(Collectors.toMap(entry -> entry.getKey().getId(), Map.Entry::getValue)));

        return form;
    }
}
