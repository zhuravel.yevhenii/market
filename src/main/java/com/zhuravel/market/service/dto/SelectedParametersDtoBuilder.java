package com.zhuravel.market.service.dto;

import com.zhuravel.market.config.MessageSourceBuilder;
import com.zhuravel.market.model.entity.LocalizedParameterType;
import com.zhuravel.market.model.entity.Parameter;
import com.zhuravel.market.model.entity.Producer;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author Evgenii Zhuravel created on 03.06.2022
 * @version 1.0
 */
public class SelectedParametersDtoBuilder {

    public static SelectedParametersDto build(List<LocalizedParameterType> parameterTypes) {
        SelectedParametersDto selectedParametersDto = new SelectedParametersDto();

        List<SelectedParametersDto.ParametersType> types = selectedParametersDto.getParameterTypes();

        for (LocalizedParameterType type: parameterTypes) {
            List<SelectedParametersDto.SelectedParameter> parameters = new ArrayList<>();

            for (Parameter parameter: type.getParameterType().getParameters()) {
                parameters.add(new SelectedParametersDto.SelectedParameter(parameter.getId(), parameter.getSense()));
            }

            types.add(new SelectedParametersDto.ParametersType(type.getLocalizedName(), parameters));
        }

        return selectedParametersDto;
    }

    public static void setProducers(SelectedParametersDto selectedParametersDto, Set<Producer> producers) {
        List<SelectedParametersDto.SelectedParameter> parameters = new ArrayList<>();

        for (Producer producer: producers) {
            parameters.add(new SelectedParametersDto.SelectedParameter(producer.getId(), producer.getName()));
        }

        MessageSource messageSource = MessageSourceBuilder.build();
        String producersName = messageSource.getMessage("label.producer", null, LocaleContextHolder.getLocale());

        selectedParametersDto.setProducers(new SelectedParametersDto.ParametersType(producersName, parameters));
    }
}
