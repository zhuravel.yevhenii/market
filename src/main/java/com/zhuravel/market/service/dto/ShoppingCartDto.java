package com.zhuravel.market.service.dto;

import java.util.Map;
import java.util.TreeMap;

/**
 * @author Evgenii Zhuravel created on 04.06.2022
 * @version 1.0
 */
public class ShoppingCartDto {

    private Long userId;

    private SelectedProductsDto selectedProducts = new SelectedProductsDto();

    private Map<ProductDto, Integer> products;

    private Map<String, Map<ProductDto, Integer>> checkouts;

    public ShoppingCartDto() {
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Map<ProductDto, Integer> getProducts() {
        return products;
    }

    public void setProducts(Map<ProductDto, Integer> products) {
        this.products = new TreeMap<>(products);
    }

    public SelectedProductsDto getSelectedProducts() {
        return selectedProducts;
    }

    public void setSelectedProducts(SelectedProductsDto selectedProducts) {
        this.selectedProducts = selectedProducts;
    }

    public Map<String, Map<ProductDto, Integer>> getCheckouts() {
        return checkouts;
    }

    public void setCheckouts(Map<String, Map<ProductDto, Integer>> checkouts) {
        this.checkouts = checkouts;
    }
}
