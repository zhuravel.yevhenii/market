package com.zhuravel.market.service.dto;

import com.zhuravel.market.model.entity.Producer;
import com.zhuravel.market.model.entity.Product;
import org.springframework.data.domain.Page;

import java.util.Set;
import java.util.TreeSet;

/**
 * @author Evgenii Zhuravel created on 30.05.2022
 * @version 1.0
 */
public class ProductsCatalogDto {

    private Page<Product> products;
    private Set<Producer> producers;

    public Page<Product> getProducts() {
        return products;
    }

    public void setProducts(Page<Product> products) {
        this.products = products;
    }

    public Set<Producer> getProducers() {
        return producers;
    }

    public void setProducers(Set<Producer> producers) {
        this.producers = new TreeSet<>(producers);
    }
}
