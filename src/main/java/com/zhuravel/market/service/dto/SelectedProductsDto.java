package com.zhuravel.market.service.dto;

import java.util.Map;
import java.util.TreeMap;

/**
 * @author Evgenii Zhuravel created on 04.06.2022
 * @version 1.0
 */
public class SelectedProductsDto {

    private Map<Long, Integer> quantities = new TreeMap<>();

    public SelectedProductsDto() {
    }

    public Map<Long, Integer> getQuantities() {
        return quantities;
    }

    public void setQuantities(Map<Long, Integer> quantities) {
        this.quantities = quantities;
    }
}
