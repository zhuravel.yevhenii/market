package com.zhuravel.market.service.dto;

import com.zhuravel.market.model.entity.Parameter;

/**
 * @author Evgenii Zhuravel created on 04.06.2022
 * @version 1.0
 */
public class ParameterDtoConverter {

    public static ParameterDto convert(Parameter parameter) {
        ParameterDto parameterDto = new ParameterDto();

        parameterDto.setId(parameter.getId())
                .setType(parameter.getType().getName())
                .setValue(parameter.getSense());

        return parameterDto;
    }
}
