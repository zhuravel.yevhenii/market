package com.zhuravel.market.service.dto;

import com.zhuravel.market.model.entity.Product;

import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Evgenii Zhuravel created on 04.06.2022
 * @version 1.0
 */
public class ProductDtoConverter {

    public static ProductDto convert(Product product) {
        ProductDto productDto = new ProductDto();

        Set<ParameterDto> parameters = product.getParameters().stream()
                .map(ParameterDtoConverter::convert)
                .collect(Collectors.toSet());

        productDto.setId(product.getId())
                .setName(product.getName())
                .setProducer(product.getProducer().getName())
                .setPrice(product.getPrice())
                .setParameters(parameters)
                .setImagePath(product.getImagePath());

        return productDto;
    }
}
