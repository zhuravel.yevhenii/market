package com.zhuravel.market.service.dto;

import java.util.Objects;

/**
 * @author Evgenii Zhuravel created on 04.06.2022
 * @version 1.0
 */
public class ParameterDto {

    private Long id;

    private String type;

    private String value;

    public Long getId() {
        return id;
    }

    public ParameterDto setId(Long id) {
        this.id = id;
        return this;
    }

    public String getType() {
        return type;
    }

    public ParameterDto setType(String type) {
        this.type = type;
        return this;
    }

    public String getValue() {
        return value;
    }

    public ParameterDto setValue(String value) {
        this.value = value;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ParameterDto that = (ParameterDto) o;
        return id.equals(that.id) && type.equals(that.type) && value.equals(that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, type, value);
    }

    @Override
    public String toString() {
        return "ParameterDto{" +
                "id=" + id +
                ", type='" + type + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
