package com.zhuravel.market.service.dto;

import java.util.Objects;
import java.util.Set;

/**
 * @author Evgenii Zhuravel created on 04.06.2022
 * @version 1.0
 */
public class ProductDto implements Comparable<ProductDto> {

    private Long id;

    private String name;

    private String producer;

    private Set<ParameterDto> parameters;

    private Long price;

    private String imagePath;

    public Long getId() {
        return id;
    }

    public ProductDto setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public ProductDto setName(String name) {
        this.name = name;
        return this;
    }

    public String getProducer() {
        return producer;
    }

    public ProductDto setProducer(String producer) {
        this.producer = producer;
        return this;
    }

    public Set<ParameterDto> getParameters() {
        return parameters;
    }

    public ProductDto setParameters(Set<ParameterDto> parameters) {
        this.parameters = parameters;
        return this;
    }

    public Long getPrice() {
        return price;
    }

    public ProductDto setPrice(Long price) {
        this.price = price;
        return this;
    }

    public String getImagePath() {
        return imagePath;
    }

    public ProductDto setImagePath(String imagePath) {
        this.imagePath = imagePath;
        return this;
    }

    @Override
    public int compareTo(ProductDto o) {
        return this.name.compareTo(o.getName());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductDto that = (ProductDto) o;
        return id.equals(that.id) && name.equals(that.name) && producer.equals(that.producer);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, producer);
    }

    @Override
    public String toString() {
        return "ProductDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", producer='" + producer + '\'' +
                ", parameters=" + parameters +
                ", price=" + price +
                ", imagePath='" + imagePath + '\'' +
                '}';
    }
}
