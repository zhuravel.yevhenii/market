DROP TABLE flyway_schema_history;
DROP TABLE shopping_cart;
DROP TABLE users;
DROP TABLE roles;
DROP TABLE user_status;
DROP TABLE category_translation;
DROP TABLE product_translation;
DROP TABLE product;
DROP TABLE producer;
DROP TABLE category;
