CREATE TABLE IF NOT EXISTS categories (
    id                 BIGINT PRIMARY KEY AUTO_INCREMENT,
    name               VARCHAR(50) NOT NULL,
    parent_category_id BIGINT,
    CONSTRAINT FK_category_category FOREIGN KEY (parent_category_id) REFERENCES categories (id)
);

CREATE TABLE IF NOT EXISTS producers (
    id            BIGINT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(50)
);

CREATE TABLE IF NOT EXISTS products (
    id          BIGINT PRIMARY KEY AUTO_INCREMENT,
    name        VARCHAR(100) NOT NULL,
    category_id BIGINT       NOT NULL,
    producer_id BIGINT,
    price       BIGINT,
    CONSTRAINT FK_product_category FOREIGN KEY (category_id) REFERENCES categories (id),
    CONSTRAINT FK_product_producer FOREIGN KEY (producer_id) REFERENCES producers (id)
);

CREATE TABLE IF NOT EXISTS roles (
    id        BIGINT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(50)
);

CREATE TABLE IF NOT EXISTS users (
    id         BIGINT PRIMARY KEY AUTO_INCREMENT,
    first_name VARCHAR(50) NOT NULL,
    last_name  VARCHAR(50) NOT NULL,
    role_id    BIGINT      NOT NULL,
    CONSTRAINT FK_user_role FOREIGN KEY (role_id) REFERENCES roles (id)
);


CREATE TABLE IF NOT EXISTS category_translations (
    category_id    BIGINT     NOT NULL,
    language_code  VARCHAR(2) NOT NULL,
    localized_name VARCHAR(100) DEFAULT '',
    localized_desc VARCHAR(100) DEFAULT '',
    CONSTRAINT PK_category_translations PRIMARY KEY (
                                                    category_id, language_code
        )
);

CREATE TABLE IF NOT EXISTS product_translations (
    product_id     BIGINT     NOT NULL,
    language_code  VARCHAR(2) NOT NULL,
    localized_name VARCHAR(100) DEFAULT '',
    localized_desc VARCHAR(100) DEFAULT '',
    CONSTRAINT PK_product_translations PRIMARY KEY (
                                                    product_id, language_code
        )
);
