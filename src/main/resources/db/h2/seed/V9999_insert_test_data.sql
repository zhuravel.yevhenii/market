INSERT INTO categories (id, name, parent_category_id)
VALUES (1002, 'Computers', 0),
       (1003, 'Phones', 0),
       (1004, 'Appliances', 0),
       (1005, 'Plumbing', 0),
--         Computers
       (2002, 'Laptops', 1002),
       (2003, 'Desktops', 1002),
       (2004, 'Monitors', 1002),
       (2005, 'Gaming', 1002),
       (2006, 'Tablets', 1002),
--         Phones
       (3002, 'Smartphones', 1003),
       (3003, 'Phone Accessories', 1003),
       (3004, 'Smart watches', 1003),
       (3005, 'Cables', 1003),
--         Appliances
       (4002, 'Large appliances', 1004),
       (4003, 'Built-in appliances', 1004),
       (4004, 'Climatic technology', 1004),
       (4005, 'Home appliances', 1004),
--         Plumbing
       (5002, 'Mixers', 1005),
       (5003, 'Kitchen sinks', 1005),
       (5004, 'Baths', 1005),
       (5005, 'Toilet bowls', 1005),
--         Computers/Desktops
       (2302, 'Monitors', 2003),
       (2303, 'Keyboards', 2003),
       (2304, 'Mouse', 2003),
       (2305, 'HDD', 2003);

INSERT INTO producers (id, name)
VALUES (1001, 'Samsung'),
       (1002, 'Dell'),
       (1003, 'Acer');
