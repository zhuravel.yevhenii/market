$(document).ready(function() {
    $("#locales").change(function () {
        var lang = $('#locales').val();

        if (lang !== ''){
            window.location.replace('?lang=' + lang);
        }
    });
});
