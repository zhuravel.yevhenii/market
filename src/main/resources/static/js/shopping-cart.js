$(document).ready(function() {
    recalculate();

    $("#shopping-cart").change(function () {
        recalculate();
    });

    function recalculate() {
        console.log("shopping-cart shopping-cart change")

        const quantityElement = document.getElementsByTagName("input");
        const unitPriceElement = document.getElementsByClassName("unit-price");
        const quantityPriceElement = document.getElementsByClassName("quantity-price");

        let subtotal = 0;
        for (let i=0; i < quantityElement.length; i++) {
            let q = parseInt(quantityElement[i].value);

            if (q < 0) {
                q = 0;
                quantityElement[i].value = q.toString();
            }

            const sum = parseFloat(unitPriceElement[i].textContent.replace(/ /g, '')) * q;

            quantityPriceElement[i].textContent = sum.toFixed(2);
            subtotal += sum;
        }

        const subtotalElement = document.getElementsByClassName("total");
        subtotalElement[0].textContent = subtotal.toFixed(2);
    }
});
